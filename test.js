const python = require('./index.js');
const path = process.cwd();

console.log(path)
python(path, 'scripts.test', 'Firs Test')
    .then(result => console.info(`Got Response:  [${typeof result}]`))
    .catch(err => console.error(`Failed to execute method: `, err));


python(path, 'cool', 'Second Test')
    .then(result => console.info(`Got Response:  [${typeof result}]`))
    .catch(err => console.error(`Failed to execute method: `, err));