const lib = require('./build/Release/python-call');

const fn = function callPythonModule(path, modulo, data) {
    return new Promise((resolve, reject) => {
        try {
            let res = lib.pythonCall(path, modulo, data);

            resolve(res);
        } catch (err) {
            reject(err);
        }
    });
};

module.exports = fn;