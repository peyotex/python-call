{
  "targets": [
    {
      "target_name": "python-call",
      "sources": [ "index.cc" ],
      "cflags!": [ "-fno-exceptions" ],
      "cflags_cc!": [ "-fno-exceptions" ],
      "defines": [ "NAPI_DISABLE_CPP_EXCEPTIONS" ],
      "include_dirs": [
        "C:\\Python39\\include",
        "<!(node -p \"require('node-addon-api').include_dir\")",
      ],
      "libraries": [
        "C:/Python39/libs/python39",
      ]
    }
  ]
}