#include <Python.h>
#include <napi.h>
#include <iostream>
#define PyString_FromString PyUnicode_FromString
Napi::Value PythonCall(const Napi::CallbackInfo& info) {
  Napi::Env env = info.Env();

  if (info.Length() < 2) {
    Napi::TypeError::New(env, "Wrong number of arguments")
        .ThrowAsJavaScriptException();
    return Napi::String::New(env, "Wrong number of arguments");
  }

  if (!info[0].IsString() || !info[1].IsString() || !info[2].IsString()) {
    Napi::TypeError::New(env, "Wrong arguments").ThrowAsJavaScriptException();
   return Napi::String::New(env, "Wrong arguments");
  }
  Napi::String path2 = info[0].As<Napi::String>();
  std::string path2_str = path2.ToString().Utf8Value();
  const char* path2_char = path2_str.c_str();

  PyObject *path_str = PyString_FromString(path2_char);
  PyObject *sys = PyImport_ImportModule("sys");                                                                                                                                                                     
  PyObject *path = PyObject_GetAttrString(sys, "path");                                                                                                                                                     
  PyList_Insert(path, 0, path_str);

  Napi::String modulo = info[1].As<Napi::String>();
  Napi::String data = info[2].As<Napi::String>();
  std::string str = modulo.ToString().Utf8Value();
  const char* module_name = str.c_str();
  PyObject *myModule;
  myModule = PyImport_ImportModule(module_name);

  PyObject *Exec = PyObject_GetAttrString(myModule, (char *)"execute");

  if (Exec == NULL)
  {
    
	  return Napi::String::New(env, "Error Module");
  }
  std::string str_data = data.ToString().Utf8Value();

  const char* module_date = str_data.c_str();

  PyObject *module_arg = Py_BuildValue("s", module_date);

  PyObject *myResult = PyObject_CallFunctionObjArgs(Exec, module_arg, NULL);

  if (PyErr_Occurred() != NULL){
    PyErr_Print();
	 return Napi::String::New(env, "Error Exec");
  }

  Py_ssize_t size = 0;
  char const * pc = PyUnicode_AsUTF8AndSize(myResult, &size);
  std::string s;
  if (pc)
	  s = std::string(pc, size);
  else
	  s = "";
  
  Napi::String result = Napi::String::New(env, s);
  Py_XDECREF(myModule);

  return result;
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
	Py_Initialize();

	PyObject *sys = PyImport_ImportModule("sys");                                                                                                                                                                     
  PyObject *path = PyObject_GetAttrString(sys, "path");                                                                                                                                                     
  PyList_Insert(path, 0, PyUnicode_FromString("."));
	exports.Set(Napi::String::New(env, "pythonCall"), Napi::Function::New(env, PythonCall));
	return exports;
}

NODE_API_MODULE(addon, Init)